import React, {Component} from 'react';
import firebase from 'firebase/app';
import 'firebase/app';
import "firebase/firestore";
import "firebase/auth";
import "firebase/functions";
import "firebase/remote-config";
import "firebase/messaging";

firebase.initializeApp({
  apiKey: "AIzaSyArRYdWbT6xh9sBu_i8nkFyfQfBdXm0Flw",
  authDomain: "frb-rn.firebaseapp.com",
  databaseURL: "https://frb-rn.firebaseio.com",
  projectId: "frb-rn",
  storageBucket: "frb-rn.appspot.com",
  messagingSenderId: "610836179477",
  appId: "1:610836179477:web:c6eeb3c7018a5f86123c1c",
  measurementId: "G-E4LSY3JNFX"
});

// Push通知関連 //////////
const messaging = firebase.messaging();
messaging.usePublicVapidKey('BDwbE5QQ6c9a1Ol5D_qxg4MtAweLF_sWKXI-8tO-RhZZQSxRi61Ct8AMOFclXU4Km5AP2v1Awo1Cs1OwKQgogok');

// Get Instance ID token. Initially this makes a network call, once retrieved
// subsequent calls to getToken will return from cache.
messaging.getToken().then((currentToken) => {
  if (currentToken) {
    console.log('currentToken: ' + currentToken);
    this.sendTokenToServer(currentToken);
    this.updateUIForPushEnabled(currentToken);
  } else {
    // Show permission request.
    console.log('No Instance ID token available. Request permission to generate one.');
    // Show permission UI.
    this.updateUIForPushPermissionRequired();
    this.setTokenSentToServer(false);
  }
}).catch((err) => {
  console.log('An error occurred while retrieving token. ', err);
  // showToken('Error retrieving Instance ID token. ', err);
  // setTokenSentToServer(false);
});

// Callback fired if Instance ID token is updated.
messaging.onTokenRefresh(() => {
  messaging.getToken().then((refreshedToken) => {
    console.log('Token refreshed.');
    // Indicate that the new Instance ID token has not yet been sent to the
    // app server.
    this.setTokenSentToServer(false);
    // Send Instance ID token to app server.
    this.sendTokenToServer(refreshedToken);
    // ...
  }).catch((err) => {
    console.log('Unable to retrieve refreshed token ', err);
    // firebase.showToken('Unable to retrieve refreshed token ', err);
  });
});
// メッセージ受信
messaging.onMessage((payload) => {
  console.log('Message received. ', payload);
  let result = payload.notification.title + '\n' + payload.notification.body;
  if (payload.data) {
    result += '\n' + payload.data.test;
    result += '\n' + payload.data.message;
  }
  alert(result);
});

// Remote Config の設定 //////////
const remoteConfig = firebase.remoteConfig();
remoteConfig.settings = {
  minimumFetchIntervalMillis: 60000,
};
remoteConfig.defaultConfig = ({
  'version': 10,
});
// アプリバージョン相当
const appVersion = 10;
let check = false;

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      loginButtonTitle: '',
      loginValue: '',
      resultValue: 'none',
      loginFlg: false
    };
    this.onChangeEmail = this.onChangeEmail.bind(this);
    this.onChangePassword = this.onChangePassword.bind(this);
    this.versionCheck();
  }

  // Remote Config から値を取得してチェックする
  versionCheck = () => {
    if (check === true) {
      return;
    }
    remoteConfig.fetchAndActivate()
    .then(() => {
      const resultString = remoteConfig.getValue('version')._value;
      const result = Number(resultString);
      if (appVersion < result) {
        // 二度通信が発生するため、表示を一度目のみにしている
        if (!check) {
          alert('新バージョンがありリースされています。\n　APP STOREからアプリの更新を行ってください');
          check = true;
        }
        console.log('version Check NG: ' + result);
      } else {
        console.log('version Check OK: ' + result);
      }
    })
    .catch((err) => {
      console.log(err);
      console.log('最新バージョン確認に失敗しました');
    });
  }

  componentDidMount() {
    // ログイン状態であれば情報が取得できる
    firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        this.setState({
          loginButtonTitle: 'ログアウト',
          loginValue: 'ログイン中',
          loginFlg: true
        });
        console.log(user);
      } else {
        this.setState({
          loginButtonTitle: 'ログイン',
          loginValue: 'ログアウト中',
          loginFlg: false
        });
      console.log('auth out');
      }
    });
  }

  onChangeEmail(event) {
    this.setState({email: event.target.value});
  }
  onChangePassword(event) {
    this.setState({password: event.target.value});
  }

  // ログイン・ログアウト
  logInOn = () => {
    if (this.state.loginFlg) {
      // ログアウト
      firebase.auth().signOut();
    } else {
      if (this.state.email === '' || this.state.password === '') {
        this.setState({loginValue: 'メールアドレスとパスワードを入力してください'});
      } else {
        // ログイン
        firebase.auth().signInWithEmailAndPassword(this.state.email, this.state.password)
        .then(() => {
          console.log('ログイン実施成功');
        })
        .catch((error) => {
          this.setState({loginValue: error.code + ' ' + error.message + ' ' + error.details});
        });
      }
    }
  }

  // ユーザー作成
  createUser = () => {
    if (this.state.email === '' || this.state.password === '') {
      this.setState({loginValue: 'メールアドレスとパスワードを入力してください'});
    } else {
      // ログイン
      firebase.auth().createUserWithEmailAndPassword(this.state.email, this.state.password)
      .then(() => {
        console.log('登録成功');
      })
      .catch((error) => {
        this.setState({loginValue: error.code + ' ' + error.message + ' ' + error.details});
      });
    }
  }

  // APIへのアクセス
  checkOnCall = () => {
    // let callBankNews = firebase.functions().httpsCallable('callBankNewsStb');
    let callBankNews = firebase.functions().httpsCallable('callFcm');
    callBankNews()
    .then((result)=>{
      console.log('callBankNews:OK');
      this.setState({resultValue: JSON.stringify(result)});
    })
    .catch((error) => {
      console.log('callBankNews:NG');
      this.setState({resultValue: 'NG: ' + error.code + ' ' + error.message});
    });
  }

  render() {
    return (
      <div>
        <div>
          <p>
            メールアドレス
            <input type="text" value={this.state.email} onChange={this.onChangeEmail} placeholder='test@test.test' />
          </p>
        </div>
        <div>
          <p>
            パスワード
            <input type="text" value={this.state.password} onChange={this.onChangePassword} placeholder='testtest' />
          </p>
        </div>
        <div>
          <button onClick={() => this.logInOn()}>{this.state.loginButtonTitle}</button>
          { !this.state.loginFlg ? <button onClick={() => this.createUser()}>登録</button> : null }
        </div>
        <div>{this.state.loginValue}</div>
        <div>
          <button onClick={() => this.checkOnCall()}>check</button>
        </div>
        <div>{this.state.resultValue}</div>
      </div>
    );
  }
}

export default App;
