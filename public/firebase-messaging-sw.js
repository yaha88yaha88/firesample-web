// Give the service worker access to Firebase Messaging.
// Note that you can only use Firebase Messaging here, other Firebase libraries
// are not available in the service worker.

// eslint-disable-next-line no-undef
importScripts('https://www.gstatic.com/firebasejs/7.20.0/firebase-app.js');
// eslint-disable-next-line no-undef
importScripts('https://www.gstatic.com/firebasejs/7.20.0/firebase-messaging.js');

// Initialize the Firebase app in the service worker by passing in
// your app's Firebase config object.
// https://firebase.google.com/docs/web/setup#config-object

firebase.initializeApp({
    apiKey: "AIzaSyArRYdWbT6xh9sBu_i8nkFyfQfBdXm0Flw",
    authDomain: "frb-rn.firebaseapp.com",
    databaseURL: "https://frb-rn.firebaseio.com",
    projectId: "frb-rn",
    storageBucket: "frb-rn.appspot.com",
    messagingSenderId: "610836179477",
    appId: "1:610836179477:web:c6eeb3c7018a5f86123c1c",
    measurementId: "G-E4LSY3JNFX"
});

// Retrieve an instance of Firebase Messaging so that it can handle background
// messages.
const messaging = firebase.messaging();

messaging.usePublicVapidKey('BDwbE5QQ6c9a1Ol5D_qxg4MtAweLF_sWKXI-8tO-RhZZQSxRi61Ct8AMOFclXU4Km5AP2v1Awo1Cs1OwKQgogok');

messaging.setBackgroundMessageHandler(function(payload) {
    console.log('[firebase-messaging-sw.js] Received background message ', payload);
    // Customize notification here
    const notificationTitle = 'Background Message Title';
    const notificationOptions = {
      body: 'Background Message body.',
      icon: '/firebase-logo.png'
    };

    // eslint-disable-next-line no-restricted-globals
    return self.registration.showNotification(notificationTitle,
      notificationOptions);
  });

// Get Instance ID token. Initially this makes a network call, once retrieved
// subsequent calls to getToken will return from cache.
messaging.getToken().then((currentToken) => {
  if (currentToken) {
    console.log('currentToken: ' + currentToken);
    // eslint-disable-next-line no-undef
    this.sendTokenToServer(currentToken);
    // eslint-disable-next-line no-undef
    this.updateUIForPushEnabled(currentToken);
  } else {
    // Show permission request.
    console.log('No Instance ID token available. Request permission to generate one.');
    // Show permission UI.
    this.updateUIForPushPermissionRequired();
    this.setTokenSentToServer(false);
  }
}).catch((err) => {
  console.log('An error occurred while retrieving token. ', err);
  // showToken('Error retrieving Instance ID token. ', err);
  // setTokenSentToServer(false);
});
